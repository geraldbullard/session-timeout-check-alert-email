// This example was for CideIgniter Framework
// check for session timeout ///////////////////////////////////////////////////////////////////////////////////////
if (isset($_GET['checksession']) && $_GET['checksession'] != '') {
    if ($this->session->userdata("userid")) {
        $response_array['status'] = 'success';
        $response_array['logged_in'] = true;
        echo json_encode($response_array);
    } else {
        $response_array['status'] = 'success';
        $response_array['logged_in'] = false;
        echo json_encode($response_array);
    }
}