function activityWatcher() {
	// The number of seconds that have passed since the user was active. Starts at 0 on page load.
	var secondsSinceLastActivity = 0;
	// Five minutes. 300 seconds.
	var maxInactivity = (300);
	// Setup the setInterval method to run
	setInterval(function() {
		secondsSinceLastActivity = secondsSinceLastActivity + 30; // add 30 seconds since last every 5 min check.
		console.log(secondsSinceLastActivity + ' seconds since the user was last active');
		// if the user has been inactive or idle for longer than the seconds (300, 5 mins) specified in maxInactivity
		if (secondsSinceLastActivity > maxInactivity) {
			// show the checks in the console for testing etc...
			console.log('User has been inactive for more than ' + maxInactivity + ' seconds');
			// now check the CI Session to be sure the user is still logged in.
			$.ajax({
				url: "/ajax.php?checksession=1", // Change this URL as per your settings
				success: function(newVal) {
					var results = JSON.parse(newVal);
					if (results.timeout !== true) { // if not logged in, show alert and send email etc...
						swal({
							title: "Session Expired! Please login again.",
							icon: "warning",
							buttons: {
								confirm: {
									value: true,
									visible: true,
									closeModal: true
								},
							},
						}).then(function(value) {
							switch (value) {
								case true:
									$.when( // do the case note email first, then redirect
										$.ajax("ajax.php?casenoteemail=1")
									).then(function(data, textStatus, jqXHR) { // redirect
										window.location = "/";
									});
									break;
								default:
							}
						});
					} else { // do nothing
					}
				}
			});
		}
	},30000);
	// The function that will be called whenever a user is active
	function activity() {
		// reset the secondsSinceLastActivity variable
		// back to 0
		secondsSinceLastActivity = 0;
	}
	// An array of DOM events that should be interpreted as
	// user activity.
	var activityEvents = ['mousedown','mousemove','keydown','scroll','touchstart'];
	// add these events to the document. register the activity function as the listener parameter.
	activityEvents.forEach(function(eventName) {
		document.addEventListener(eventName, activity,true);
	});
}
activityWatcher();